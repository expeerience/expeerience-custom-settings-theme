import { withPluginApi } from "discourse/lib/plugin-api";
import { on } from "discourse-common/utils/decorators";
import { setDefaultHomepage } from 'discourse/lib/utilities';

export default {
  name: "show-footer-on-static-pages",
  initialize() {
   
    if (settings.home_url_override) {
      setDefaultHomepage(settings.home_url_override);
    }

    withPluginApi("0.8", api => {
      const splitIcons = settings.icons_to_override.split("|").filter(Boolean)
      splitIcons.forEach(icon =>{
        const oldIcon = icon[0]
        const newIcon = icon[1]
        api.replaceIcon(oldIcon, newIcon)      
      })
      
    });
  }
};
